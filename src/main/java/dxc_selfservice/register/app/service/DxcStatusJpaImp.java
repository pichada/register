package dxc_selfservice.register.app.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import dxc_selfservice.register.app.domain.DxcStatus;
import dxc_selfservice.register.app.util.DxcStatusFactory;
import dxc_selfservice.register.infra.entity.DxcStatusEntity;
import dxc_selfservice.register.infra.repository.DxcStatusApiRepository;

public class DxcStatusJpaImp implements DxcStatusService {
	
	@Autowired
	private DxcStatusApiRepository dxcstatusRepo;
	
	@Autowired
	private DxcStatusFactory dxcstatusFactory;

	@Override
	public List<DxcStatus> findAllStatus() {
		List<DxcStatus> dxcStatusList = null;
		Iterable<DxcStatusEntity> dxcEntityIterator = dxcstatusRepo.findAll();
		dxcStatusList = dxcstatusFactory.createDxcStatusList(dxcEntityIterator);		
		return dxcStatusList; 
	}

	@Override
	public void addInsert(DxcStatus status) {
		// TODO Auto-generated method stub
		DxcStatusEntity dxcstatusEntity = dxcstatusFactory.createDxcStatusEntity(status);
		dxcstatusRepo.save(dxcstatusEntity);
	}

	@Override
	public String update(DxcStatus status) {
		// TODO Auto-generated method stub
		DxcStatusEntity dxcstatusEntity = dxcstatusFactory.createDxcStatusEntity(status);
		dxcstatusRepo.save(dxcstatusEntity);
		return "Update succes";
	}

	@Override
	public String delete(DxcStatus status) {
		// TODO Auto-generated method stub
		DxcStatusEntity dxcstatusEntity = dxcstatusFactory.createDxcStatusEntity(status);
		dxcstatusRepo.delete(dxcstatusEntity);
		return "Delete succes";
	}





}
