package dxc_selfservice.register.app.service;

import java.util.List;

import dxc_selfservice.register.app.domain.DxcStatus;

public interface DxcStatusService {
	List<DxcStatus>findAllStatus();	
	public void addInsert(DxcStatus status);
	public String update(DxcStatus status);
	public String delete(DxcStatus status);

}
