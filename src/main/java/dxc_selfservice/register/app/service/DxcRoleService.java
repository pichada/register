package dxc_selfservice.register.app.service;

import java.util.List;

import dxc_selfservice.register.app.domain.DxcRole;

public interface DxcRoleService {
	List<DxcRole>findAllRole();	

	public String addInsert(DxcRole role);
	public String update(DxcRole role);
	public String delete(DxcRole role);
}
