package dxc_selfservice.register.app.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import dxc_selfservice.register.app.domain.DxcStatus;
import dxc_selfservice.register.infra.entity.DxcStatusEntity;

@Component
public class DxcStatusFactory {
	public List<DxcStatus>createDxcStatusList(Iterable<DxcStatusEntity>dxcstatusEntityiterator)
	{
		List<DxcStatus> dxcstatusList = null;		
		if(dxcstatusEntityiterator != null) {			
			dxcstatusList = new ArrayList<DxcStatus>();			
			for(Iterator<DxcStatusEntity> iterator = dxcstatusEntityiterator.iterator(); iterator.hasNext();) {
				DxcStatusEntity dxcStatusEntity = iterator.next();
				DxcStatus dxcStatus = createDxcStatus(dxcStatusEntity);
				dxcstatusList.add(dxcStatus);
			}			
		}
		return dxcstatusList;
	}

	private DxcStatus createDxcStatus(DxcStatusEntity dxcStatusEntity) {

		DxcStatus dxcStatus = null;
		if(dxcStatusEntity!=null) {
			dxcStatus = new DxcStatus();
			dxcStatus.setId(dxcStatusEntity.getId());
			dxcStatus.setStatus(dxcStatusEntity.getStatus());
			dxcStatus.setStatuscode(dxcStatusEntity.getStatuscode());
			dxcStatus.setStatusname(dxcStatusEntity.getStatusname());			
		}
		return dxcStatus;
	}

	public DxcStatusEntity createDxcStatusEntity(DxcStatus status) {
		// TODO Auto-generated method stub
		DxcStatusEntity dxcstatusEntity = null;
		if(status != null) {
			dxcstatusEntity = new DxcStatusEntity();
			dxcstatusEntity.setId(status.getId());
			dxcstatusEntity.setStatus(status.getStatus());
			dxcstatusEntity.setStatuscode(status.getStatuscode());
			dxcstatusEntity.setStatusname(status.getStatusname());
		}
		return dxcstatusEntity;
	}
	

}
