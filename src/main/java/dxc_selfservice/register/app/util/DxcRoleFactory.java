package dxc_selfservice.register.app.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import dxc_selfservice.register.app.domain.DxcRole;
import dxc_selfservice.register.app.domain.DxcStatus;
import dxc_selfservice.register.infra.entity.DxcRoleEntity;
import dxc_selfservice.register.infra.entity.DxcStatusEntity;

public class DxcRoleFactory {
	public List<DxcRole>createRoleList(Iterable<DxcRoleEntity>dxcroleEntityiterator){
		List<DxcRole> dxcroleList = null;
		if(dxcroleEntityiterator != null) {
			dxcroleList = new ArrayList<DxcRole>();			
			for(Iterator<DxcRoleEntity> iterator = dxcroleEntityiterator.iterator(); iterator.hasNext();) {
				DxcRoleEntity dxcRoleEntity = iterator.next();
				DxcRole dxcRole = createDxcRole(dxcRoleEntity);
				dxcroleList.add(dxcRole);
			}			
		}
		return dxcroleList;
	}

	private DxcRole createDxcRole(DxcRoleEntity dxcRoleEntity) {
		// TODO Auto-generated method stub
		DxcRole dxcRole = null;
		if(dxcRoleEntity!=null) {
			dxcRole = new DxcRole();
			dxcRole.setId(dxcRoleEntity.getId());
			dxcRole.setNameTh(dxcRoleEntity.getNameTh());
			dxcRole.setNameEng(dxcRoleEntity.getNameEng());
			dxcRole.setInheritanceRoleId(dxcRoleEntity.getInheritanceRoleId());	
			dxcRole.setCreateDate(dxcRoleEntity.getCreateDate());
			dxcRole.setExprDate(dxcRoleEntity.getExprDate());
			dxcRole.setCreaterid(dxcRoleEntity.getCreaterid());
			dxcRole.setCreaterName(dxcRoleEntity.getCreaterName());
			dxcRole.setDescription(dxcRoleEntity.getDescription());
		}
		return dxcRole;
	}
	
	public DxcRoleEntity createDxcRoleEntity(DxcRole role) {
		// TODO Auto-generated method stub
		DxcRoleEntity dxcroleEntity = null;
		if(role != null) {
			dxcroleEntity = new DxcRoleEntity();
			dxcroleEntity.setId(role.getId());
			dxcroleEntity.setNameTh(role.getNameTh());
			dxcroleEntity.setNameEng(role.getNameEng());
			dxcroleEntity.setInheritanceRoleId(role.getInheritanceRoleId());
			dxcroleEntity.setCreateDate(role.getCreateDate());
			dxcroleEntity.setExprDate(role.getExprDate());
			dxcroleEntity.setCreaterid(role.getCreaterid());
			dxcroleEntity.setCreaterName(role.getCreaterName());
			dxcroleEntity.setDescription(role.getDescription());
			
		}
		return dxcroleEntity;
	}


}
