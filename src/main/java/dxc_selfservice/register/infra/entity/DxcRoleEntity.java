package dxc_selfservice.register.infra.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name ="ROLE")
public class DxcRoleEntity {
	@Id
	private Integer id;
	@Column(name="[nameth]")
	private String nameTh;
	@Column(name="[nameeng]")
	private String nameEng;
	@Column(name="[inheritanceroleId]")
	private Integer inheritanceRoleId;
	@Column(name="[createdate]")
	private Date createDate;
	@Column(name="[exprdate]")
	private Date exprDate;
	@Column(name="[createrid]")
	private Integer createrid;
	@Column(name="[creatername]")
	private String createrName;
	@Column(name="[description]")
	private String description;
	public DxcRoleEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DxcRoleEntity(Integer id, String nameTh, String nameEng, Integer inheritanceRoleId, Date createDate,
			Date exprDate, Integer createrid, String createrName, String description) {
		super();
		this.id = id;
		this.nameTh = nameTh;
		this.nameEng = nameEng;
		this.inheritanceRoleId = inheritanceRoleId;
		this.createDate = createDate;
		this.exprDate = exprDate;
		this.createrid = createrid;
		this.createrName = createrName;
		this.description = description;
	}
	@Override
	public String toString() {
		return "DxcRoleEntity [id=" + id + ", nameTh=" + nameTh + ", nameEng=" + nameEng + ", inheritanceRoleId="
				+ inheritanceRoleId + ", createDate=" + createDate + ", exprDate=" + exprDate + ", createrid="
				+ createrid + ", createrName=" + createrName + ", description=" + description + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((createrName == null) ? 0 : createrName.hashCode());
		result = prime * result + ((createrid == null) ? 0 : createrid.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((exprDate == null) ? 0 : exprDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inheritanceRoleId == null) ? 0 : inheritanceRoleId.hashCode());
		result = prime * result + ((nameEng == null) ? 0 : nameEng.hashCode());
		result = prime * result + ((nameTh == null) ? 0 : nameTh.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DxcRoleEntity other = (DxcRoleEntity) obj;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (createrName == null) {
			if (other.createrName != null)
				return false;
		} else if (!createrName.equals(other.createrName))
			return false;
		if (createrid == null) {
			if (other.createrid != null)
				return false;
		} else if (!createrid.equals(other.createrid))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (exprDate == null) {
			if (other.exprDate != null)
				return false;
		} else if (!exprDate.equals(other.exprDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inheritanceRoleId == null) {
			if (other.inheritanceRoleId != null)
				return false;
		} else if (!inheritanceRoleId.equals(other.inheritanceRoleId))
			return false;
		if (nameEng == null) {
			if (other.nameEng != null)
				return false;
		} else if (!nameEng.equals(other.nameEng))
			return false;
		if (nameTh == null) {
			if (other.nameTh != null)
				return false;
		} else if (!nameTh.equals(other.nameTh))
			return false;
		return true;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNameTh() {
		return nameTh;
	}
	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}
	public String getNameEng() {
		return nameEng;
	}
	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}
	public Integer getInheritanceRoleId() {
		return inheritanceRoleId;
	}
	public void setInheritanceRoleId(Integer inheritanceRoleId) {
		this.inheritanceRoleId = inheritanceRoleId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getExprDate() {
		return exprDate;
	}
	public void setExprDate(Date exprDate) {
		this.exprDate = exprDate;
	}
	public Integer getCreaterid() {
		return createrid;
	}
	public void setCreaterid(Integer createrid) {
		this.createrid = createrid;
	}
	public String getCreaterName() {
		return createrName;
	}
	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
	
}
