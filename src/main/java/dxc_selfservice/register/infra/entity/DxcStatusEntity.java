package dxc_selfservice.register.infra.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DXC_STATUS")
public class DxcStatusEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	String statuscode;
	String statusname;
	String status;
	public DxcStatusEntity(Long id, String statuscode, String statusname, String status) {
		super();
		this.id = id;
		this.statuscode = statuscode;
		this.statusname = statusname;
		this.status = status;
	}
	public DxcStatusEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DxcStatusEntity other = (DxcStatusEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statuscode == null) {
			if (other.statuscode != null)
				return false;
		} else if (!statuscode.equals(other.statuscode))
			return false;
		if (statusname == null) {
			if (other.statusname != null)
				return false;
		} else if (!statusname.equals(other.statusname))
			return false;
		return true;
	}
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((statuscode == null) ? 0 : statuscode.hashCode());
		result = prime * result + ((statusname == null) ? 0 : statusname.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "DxcStatus [id=" + id + ", statuscode=" + statuscode + ", statusname=" + statusname + ", status="
				+ status + "]";
	}
	
	
}
