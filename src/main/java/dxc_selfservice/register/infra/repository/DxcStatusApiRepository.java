package dxc_selfservice.register.infra.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import dxc_selfservice.register.infra.entity.DxcStatusEntity;

public interface DxcStatusApiRepository extends PagingAndSortingRepository<DxcStatusEntity, Integer>{

}
