package dxc_selfservice.register.infra.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import dxc_selfservice.register.infra.entity.DxcRoleEntity;

public interface DxcRoleApiRepository extends PagingAndSortingRepository<DxcRoleEntity, Integer>{

}
