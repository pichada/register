package dxc_selfservice.register.api.dto;

public class DxcStatusDto {
	Long id;
	String statuscode;
	String statusname;
	String status;
	public DxcStatusDto(Long id, String statuscode, String statusname, String status) {
		super();
		this.id = id;
		this.statuscode = statuscode;
		this.statusname = statusname;
		this.status = status;
	}
	public DxcStatusDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "DxcStatusDto [id=" + id + ", statuscode=" + statuscode + ", statusname=" + statusname + ", status="
				+ status + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((statuscode == null) ? 0 : statuscode.hashCode());
		result = prime * result + ((statusname == null) ? 0 : statusname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DxcStatusDto other = (DxcStatusDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statuscode == null) {
			if (other.statuscode != null)
				return false;
		} else if (!statuscode.equals(other.statuscode))
			return false;
		if (statusname == null) {
			if (other.statusname != null)
				return false;
		} else if (!statusname.equals(other.statusname))
			return false;
		return true;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

}
