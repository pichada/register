package dxc_selfservice.register.api.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dxc_selfservice.register.api.dto.DxcRoleDto;
import dxc_selfservice.register.app.domain.DxcRole;

public class DxcRoleDtoFactory {
	private final static Logger LOGGER= LoggerFactory.getLogger(DxcRoleDtoFactory.class);
	public DxcRoleDto createDxcRoleDto(DxcRole dxcRole ) {
		DxcRoleDto dxcRoleDto = new DxcRoleDto();
		dxcRoleDto.setId(dxcRole.getId());
		dxcRoleDto.setNameTh(dxcRole.getNameTh());
		dxcRoleDto.setNameEng(dxcRole.getNameEng());
		dxcRoleDto.setInheritanceRoleId(dxcRole.getInheritanceRoleId());
		dxcRoleDto.setCreateDate(dxcRole.getCreateDate());
		dxcRoleDto.setExprDate(dxcRole.getExprDate());
		dxcRoleDto.setCreaterid(dxcRole.getCreaterid());
		dxcRoleDto.setCreaterName(dxcRole.getCreaterName());
		dxcRoleDto.setDescription(dxcRole.getDescription());
		return dxcRoleDto;
	}
	public List<DxcRoleDto>createDxcRoleDtoList(List<DxcRole> dxcRoleList){
		List<DxcRoleDto>dxcRoleDtoList = new ArrayList<DxcRoleDto>();
		for (Iterator<DxcRole> iterator = dxcRoleList.iterator(); iterator.hasNext();) {
			DxcRole dxcrole =  iterator.next();
			LOGGER.info("Values : "+dxcrole);
			DxcRoleDto dxcRoleDto = createDxcRoleDto(dxcrole);
			dxcRoleDtoList.add(dxcRoleDto);
		}
		return dxcRoleDtoList;
	}
}
