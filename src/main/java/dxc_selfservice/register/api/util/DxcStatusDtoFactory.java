package dxc_selfservice.register.api.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import dxc_selfservice.register.api.dto.DxcStatusDto;
import dxc_selfservice.register.app.domain.DxcStatus;


@Component
public class DxcStatusDtoFactory {
private final static Logger LOGGER= LoggerFactory.getLogger(DxcStatusDtoFactory.class);
	
	public DxcStatusDto createDxcStatusDto(DxcStatus dxcStatus ) {
		DxcStatusDto dxcStatusDto = new DxcStatusDto();
		dxcStatusDto.setId(dxcStatus.getId());
		dxcStatusDto.setStatus(dxcStatus.getStatus());
		dxcStatusDto.setStatuscode(dxcStatus.getStatuscode());
		dxcStatusDto.setStatusname(dxcStatus.getStatusname());
		return dxcStatusDto;
	}
	public List<DxcStatusDto>createDxcStatusDtoList(List<DxcStatus> dxcStatusList){
		List<DxcStatusDto>dxcStatusDtoList = new ArrayList<DxcStatusDto>();
		for (Iterator<DxcStatus> iterator = dxcStatusList.iterator(); iterator.hasNext();) {
			DxcStatus dxcstatus =  iterator.next();
			LOGGER.info("Values : "+dxcstatus);
			DxcStatusDto dxcStatusDto = createDxcStatusDto(dxcstatus);
			dxcStatusDtoList.add(dxcStatusDto);
		}
		return dxcStatusDtoList;
	}
}
