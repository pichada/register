package dxc_selfservice.register.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dxc_selfservice.register.api.dto.DxcRoleDto;
import dxc_selfservice.register.api.dto.DxcStatusDto;
import dxc_selfservice.register.api.util.DxcRoleDtoFactory;
import dxc_selfservice.register.app.domain.DxcRole;
import dxc_selfservice.register.app.domain.DxcStatus;
import dxc_selfservice.register.app.service.DxcRoleService;

@RestController
@RequestMapping("/api/DxcRole")
public class DxcRoleController {
	@Autowired
		private DxcRoleService service;
	@Autowired
		private DxcRoleDtoFactory dtoFactory;
	@RequestMapping(value = "/Role/", method = RequestMethod.GET)
	public List<DxcRoleDto> findAllRole(){
			 List<DxcRoleDto> resulList = null;
			 List<DxcRole> role = service.findAllRole();
			 resulList = dtoFactory.createDxcRoleDtoList(role);
				return resulList;
	}
	//ต้องการเพิ่มข้อมูลในตาราง Status 
	@RequestMapping(value="/add", method = RequestMethod.POST)
	@ResponseBody
	public List<DxcRoleDto> addRole(@RequestBody DxcRole role) {
		List<DxcRoleDto> resuleList = null;
		service.addInsert(role);
		System.out.println("Success");
		return null;
		
	}
	//อัปเดตค่าในตาราง Status
	@RequestMapping(value="/update", method = RequestMethod.PUT)
	@ResponseBody
	public String update(@RequestBody DxcRole role){
		String updateMessage = service.update(role);
		return updateMessage;
	}
	//ลบข้อมูลในตาราง Status 1 แถว 
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(@RequestBody DxcRole role){
		String updateMessage = service.delete(role);
		return updateMessage;
	}

}
