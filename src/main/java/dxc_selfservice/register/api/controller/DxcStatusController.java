package dxc_selfservice.register.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dxc_selfservice.register.api.dto.DxcStatusDto;
import dxc_selfservice.register.api.util.DxcStatusDtoFactory;
import dxc_selfservice.register.app.domain.DxcStatus;
import dxc_selfservice.register.app.service.DxcStatusService;

@RestController
@RequestMapping("/api/DxcStatus")
public class DxcStatusController {
	@Autowired
	private DxcStatusService service;
	@Autowired
	private DxcStatusDtoFactory dtoFactory;
	@RequestMapping(value = "/Status/", method = RequestMethod.GET)
	public List<DxcStatusDto> findAllStatus()	{
			  List<DxcStatusDto> resultList = null;
			  List<DxcStatus> status = service.findAllStatus();
				resultList = dtoFactory.createDxcStatusDtoList(status);
		return resultList;
	}

	//ต้องการเพิ่มข้อมูลในตาราง Status 
	@RequestMapping(value="/add", method = RequestMethod.POST)
	@ResponseBody
	public List<DxcStatusDto> addStatus(@RequestBody DxcStatus status) {
		List<DxcStatusDto> resuleList = null;
		service.addInsert(status);
		System.out.println("Success");
		return null;
		
	}
	//อัปเดตค่าในตาราง Status
	@RequestMapping(value="/update", method = RequestMethod.PUT)
	@ResponseBody
	public String update(@RequestBody DxcStatus status){
		String updateMessage = service.update(status);
		return updateMessage;
	}
	//ลบข้อมูลในตาราง Status 1 แถว 
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(@RequestBody DxcStatus status){
		String updateMessage = service.delete(status);
		return updateMessage;
	}

}
