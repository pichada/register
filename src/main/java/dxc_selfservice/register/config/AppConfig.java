package dxc_selfservice.register.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dxc_selfservice.register.app.service.DxcStatusJpaImp;
import dxc_selfservice.register.app.service.DxcStatusService;

@Configuration
public class AppConfig {
	@Bean
	public DxcStatusService dxcStatusService() {
		return new DxcStatusJpaImp();
	}
} 
